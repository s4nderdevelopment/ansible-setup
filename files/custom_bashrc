#!/bin/bash

##########
# Python #
##########
export PATH=$PATH:/home/s4nder/.local/bin

################################
# Snap package manager (snapd) #
################################
export PATH=$PATH:/snap/bin

##############
# Dotnet CLI #
##############
# Opt out of dotnet CLI telemetry
export DOTNET_CLI_TELEMETRY_OPTOUT=1

#######
# Git #
#######
# Alias log1 to a nice tree log globally
git config --global alias.log1 "log --graph --pretty=oneline --abbrev-commit --decorate"

########
# Bash #
########

# Aliases
alias llah='ls -lah'
alias lslah='llah'
alias ll='ls -l'
alias lla='ls -la'
alias la='ls -la'

alias python="python3"

# Show git branch
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

ps1_git_branch(){
    export PS1="\[\e]0;\w\a\]\n\[\e[37m\]\u\[\e[33m\]@\[\e[36m\]\h \[\e[37m\]\w\[\e[33m\]$(parse_git_branch)\n\[\e[31m\]\$ \[\e[0m\]"
}

export PROMPT_COMMAND="ps1_git_branch"

# Tab-autocomplete: case insensitive
bind 'set completion-ignore-case on'

# Grep
alias grep='grep --color=auto'

# Open (multiple) files from terminal with 'bulkopen ./*.txt'
function bulkopen {
        for item in "$@"; do
                item_abs=$(realpath -- "$item")
                xdg-open "$item_abs" & disown
        done
}

alias open='bulkopen'

function cliptofile {
        if [[ $XDG_SESSION_TYPE == "x11" ]] ; then
                found_mimetype=""
                prefer_mimetypes=("image/png" "text/plain")
                available_mimetypes=$(xclip -selection clipboard -t TARGETS -o)
                for type in ${prefer_mimetypes[@]}; do
                        echo $available_mimetypes | grep -q -- "$type"
                        retval=$?
                        if [ $retval -eq 0 ] ; then
                                found_mimetype="$type"
                                break
                        fi
                done
                if [ "$#" -lt 1 ] || [ -z "$1" ] ; then
                        echo "Specify a filename, and optionally a mimetype. (Default mimetype = autoselect, currently '${found_mimetype:-none}')."
                        echo "cliptofile <filename> [mimetype]"
                else
                        outfile="$1"
                        found_mimetype="${found_mimetype:-text/plain}"
                        mimetype="${2:-$found_mimetype}"
                        echo "Using mimetype: $mimetype"
                        xclip -selection clipboard -t "$mimetype" -o > "$outfile"
                fi
        else
                if [ "$#" -lt 1 ] || [ -z "$1" ] ; then
                        echo "Specify a filename. Mimetype is determined automatically (based on file extension)."
                        echo "cliptofile <filename>"
                else
                        outfile="$1"
                        wl-paste > "$outfile"
                fi
        fi
}

function cliptofiletext {
        cliptofile "$1" "text/plain"
}

alias clip2file="cliptofile"
alias clip2filetext="cliptofiletext"
