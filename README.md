# Ansible Setup

My custom Ansible setup playbooks for a new laptop/pc or server (VPS) running Linux.

## Disclaimer

This setup script changes some default settings for your system (like nanorc).
Run at your own risk. If you have preconfigured settings, they might be overwritten by this playbook.

## Getting started

<span style="color: orange;">Note: Currently only apt and dnf based distros (e.g. Debian/Ubuntu ane Fedora/RHEL) are supported by the playbooks.</span>

To set up, use the following commands:

```bash
# Clone the repository (with depth 1 for fast clone)
git clone --depth 1 https://gitlab.com/s4nderdevelopment/ansible-setup
# Go into the cloned directory
cd ansible-setup
# Make the install script executable
chmod +x ./install.sh
# Run the install script, which installs ansible and runs the local.yml playbook
```

To run playbooks:

```bash
sudo ./install.sh default_packages.yml # [...other_playbooks.yml]
```

## Todo list

- [ ] Fix bashrc
- [ ] Fix nanorc
