if [ ! $UID = 0 ]; then
    echo "Please run this script as root."
    exit
fi

if [ "$#" -lt 1 ] ; then
    echo "Please use: $0 <playbook> [playbooks...]"
    exit
fi

ansible-playbook -b $@
